package ru.yandex.tests;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

@Feature("Тестовое задание Yandex Money")
@Story("ТЗ выполненное на Java")
public class YandexAuthTest extends BaseTest {

    @Value("${ru.yandex.money.java.test.username}")
    private String username;

    @Value("${ru.yandex.money.java.test.password}")
    private String password;

    @Test
    @DisplayName("Юзер логаут тест")
    public void logoutTest() {
        actions.clickLoginButton();
        actions.enterUserNameAndClickOnSubmitButton(username);
        actions.enterPasswordAndClickOnSubmitButton(password);
        actions.clickOnUserInfo();
        actions.logoutFromAccount();
        assertions.verifyUserSuccesfullyLogout();
    }

    @Test
    @DisplayName("Юзер логин тест")
    public void loginTest() {
        actions.clickLoginButton();
        actions.enterUserNameAndClickOnSubmitButton(username);
        actions.enterPasswordAndClickOnSubmitButton(password);
        assertions.verifyUserIsSucceesfullyLogined();
        assertions.userWithNameSuccesfullyLogined(username);
    }

}

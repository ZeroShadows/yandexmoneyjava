package ru.yandex.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.yandex.steps.Actions;
import ru.yandex.steps.Assertions;
import ru.yandex.config.AppConfig;

import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
public abstract class BaseTest {

    @Autowired
    Actions actions;

    @Autowired
    Assertions assertions;

    @Before
    public void testSetUp() {
        actions.openMainPage();
    }
}

package ru.yandex.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackages = {"ru.yandex"})
@Import({WebDriverConfiguration.class})
@PropertySource("classpath:conf/yandexmoney.properties")
public class AppConfig {
}

package ru.yandex.steps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import ru.yandex.model.pages.HomePage;
import ru.yandex.model.pages.MainPage;
import ru.yandex.model.pages.PassportPage;

public class Background {

    @Autowired
    @Lazy
    MainPage mainPage;

    @Autowired
    @Lazy
    HomePage homePage;

    @Autowired
    @Lazy
    PassportPage passportPage;

}

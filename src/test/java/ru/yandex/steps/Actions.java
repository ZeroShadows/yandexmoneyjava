package ru.yandex.steps;

import io.qameta.allure.Step;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
@Lazy
public class Actions extends Background {

    @Step("Открыта главная страница")
    public void openMainPage() {
        mainPage.openMainPage();
    }

    @Step("Ввожу имя пользователя {0} на странице паспорта")
    public void enterUserNameAndClickOnSubmitButton(String userName) {
        passportPage.enterUsername(userName);
        passportPage.clickSumbitButton();
    }

    @Step("Ввожу пароль {0} на странице паспорта")
    public void enterPasswordAndClickOnSubmitButton(String password) {
        passportPage.enterPassword(password);
        passportPage.clickSumbitButton();
    }

    @Step("Нажимаю на информацию о пользователе")
    public void clickOnUserInfo() {
        homePage.clickOnUserInfo();
    }

    @Step("Выхожу из аккаунта")
    public void logoutFromAccount() {
        homePage.clickOnLogOutButton();
    }

    @Step("Жму на кнопку логина на главной странице")
    public void clickLoginButton() {
        mainPage.clickLoginButton();
    }

}

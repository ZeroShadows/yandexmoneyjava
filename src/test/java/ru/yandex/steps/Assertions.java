package ru.yandex.steps;

import io.qameta.allure.Step;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@Service
@Lazy
public class Assertions extends Background {

    @Step("Проверка пользовательского разлогина")
    public void verifyUserSuccesfullyLogout() {
        assertThat("Пользователь не разлогинился", mainPage.promoBannerIsDisplayed());
    }

    @Step("Проверка пользовательского логина")
    public void verifyUserIsSucceesfullyLogined() {
        assertThat("Пользователь не залогинился", homePage.userInfoIsDisplayed());
    }

    @Step("Юзер с именем {0} успешно залогинился")
    public void userWithNameSuccesfullyLogined(String username) {
        assertThat("Имя юзера не совпадает", homePage.getUserName(), equalTo(username));
    }

}

package ru.yandex.config;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import ru.yandex.webdriver.WebDriverWrapper;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static org.openqa.selenium.remote.BrowserType.CHROME;
import static org.openqa.selenium.remote.BrowserType.FIREFOX;

@Configuration
@Lazy
public class WebDriverConfiguration {

    @Value("${ru.yandex.money.java.test.browser}")
    private String browserName;

    @Bean(name = "driverWrapper", destroyMethod = "quit")
    public WebDriverWrapper webDriverWrapper() {
        return Match(browserName).of(
                Case($(CHROME::equalsIgnoreCase), this::initChrome),
                Case($(FIREFOX::equalsIgnoreCase), this::initFirefox)
        );
    }

    private WebDriverWrapper initChrome() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return new WebDriverWrapper(driver);
    }

    private WebDriverWrapper initFirefox() {
        WebDriverManager.firefoxdriver().setup();
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        return new WebDriverWrapper(driver);
    }

}

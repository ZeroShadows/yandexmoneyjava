package ru.yandex.model.common;

import org.springframework.beans.factory.annotation.Autowired;
import ru.yandex.webdriver.WebDriverHelper;

public abstract class AbstractPage {

    @Autowired
    protected WebDriverHelper driver;

    public abstract boolean isDisplayed();
}

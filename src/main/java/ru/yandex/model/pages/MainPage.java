package ru.yandex.model.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Value;
import ru.yandex.model.common.AbstractPage;
import ru.yandex.model.common.PageObject;

@PageObject
public class MainPage extends AbstractPage {

    @Value("${ru.yandex.money.java.test.link}")
    private String url;

    @FindBy(className = "qa-money-logo")
    private WebElement logo;

    @FindBy(className = "header2__button")
    private WebElement loginButton;

    @FindBy(className = "promo-screen__content")
    private WebElement promoBanner;


    public void openMainPage() {
        driver.navigate(url);
    }

    public void clickLoginButton() {
        driver.click(loginButton);
    }

    public boolean promoBannerIsDisplayed() {
        driver.waitForElementDisplayed(promoBanner, 10);
        return driver.isDisplayed(promoBanner);
    }

    public boolean loginButtonIsDisplayed() {
        return driver.isDisplayed(loginButton);
    }

    @Override
    public boolean isDisplayed() {
        return driver.isDisplayed(logo);
    }
}

package ru.yandex.model.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.model.common.AbstractPage;
import ru.yandex.model.common.PageObject;

@PageObject
public class PassportPage extends AbstractPage {

    @FindBy(xpath = "//input[@name='login']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@name='passwd']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitButton;

    @FindBy(className = "logo logo_name")
    private WebElement passportLogo;

    public void enterUsername(String username) {
        driver.typeText(loginField, username);
    }

    public void enterPassword(String password) {
        driver.typeText(passwordField, password);
    }

    public void clickSumbitButton() {
        driver.click(submitButton);
    }

    @Override
    public boolean isDisplayed() {
        return driver.isDisplayed(passportLogo);
    }
}

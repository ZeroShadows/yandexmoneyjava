package ru.yandex.model.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Value;
import ru.yandex.model.common.AbstractPage;
import ru.yandex.model.common.PageObject;

@PageObject
public class HomePage extends AbstractPage {

    @Value("${ru.yandex.money.java.test.timeoutInSeconds}")
    private int timeoutInSeconds;

    @FindBy(className = "qa-user-show-sidebar-avatar")
    private WebElement userInfo;

    @FindBy(css = ".qa-user-exit button")
    private WebElement logoutButton;

    @FindBy(className = "qa-user-name")
    private WebElement userNameFromUserInfo;

    public boolean userInfoIsDisplayed() {
        driver.waitForElementDisplayed(userInfo, timeoutInSeconds);
        return driver.isDisplayed(userInfo);
    }

    public void clickOnUserInfo() {
        driver.click(userInfo);
    }

    public void clickOnLogOutButton() {
        driver.click(logoutButton);
    }

    public String getUserName() {
        clickOnUserInfo();
        driver.waitForElementDisplayed(userNameFromUserInfo, timeoutInSeconds);
        return userNameFromUserInfo.getText();
    }

    @Override
    public boolean isDisplayed() {
        return driver.isDisplayed(userInfo);
    }
}

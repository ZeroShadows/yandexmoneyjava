package ru.yandex.webdriver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@Lazy
public class WebDriverHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebDriverHelper.class);
    private static final String LOGGER_INFO_TEMPLATE = "Event: {}; ExecutionTime(ms): {}; PageURL: {}";

    @Value("${ru.yandex.money.java.test.timeoutInSeconds}")
    private int timeoutInSeconds;

    @Value("${ru.yandex.money.java.test.sleepInMillis}")
    private int sleepInMillis;

    @Autowired
    @Lazy
    @Qualifier("driverWrapper")
    private WebDriverWrapper driverWrapper;

    public void navigate(String url) {
        long startTime = System.currentTimeMillis();
        driverWrapper.navigate().to(url);
        logInfo("Navigate to URL: " + url, startTime);
    }

    public String getCurrentUrl() {
        return driverWrapper.getCurrentUrl();
    }

    public void click(WebElement element) {
        long startTime = System.currentTimeMillis();
        try {
            waitForElementDisplayed(element, timeoutInSeconds);
            waitForElementClickable(element, timeoutInSeconds);
            scrollTo(element);
            element.click();
        } catch (Exception e) {
            logException(e, startTime);
            throw e;
        } finally {
            logInfo("Click WebElement", startTime);
        }
    }

    public void typeText(WebElement element, String text) {
        long startTime = System.currentTimeMillis();
        try {
            waitForElementDisplayed(element, timeoutInSeconds);
            scrollTo(element);
            element.clear();
            element.sendKeys(text);
        } catch (Exception e) {
            logException(e, startTime);
            throw e;
        } finally {
            logInfo("Type text into WebElement: " + text, startTime);
        }
    }

    private void scrollTo(WebElement webElement) {
        ((JavascriptExecutor) driverWrapper.getDriver()).executeScript("arguments[0].scrollIntoView(false)", webElement);
    }

    public boolean isDisplayed(WebElement element) {
        long startTime = System.currentTimeMillis();
        boolean displayed = false;

        try {
            displayed = element.isDisplayed();
        } catch (Exception e) {
            logException(e, startTime);
        }
        return displayed;
    }

    public String getPageTitle() {
        return driverWrapper.getTitle();
    }

    /** waiters **/

    public void waitForElementClickable(WebElement element, int timeOutInSeconds) {
        new WebDriverWait(driverWrapper.getDriver(), timeOutInSeconds).until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForElementDisplayed(WebElement element, int timeOutInSeconds) {
        new WebDriverWait(driverWrapper.getDriver(), timeOutInSeconds).until(ExpectedConditions.visibilityOf(element));
    }

    /** loggers **/

    private void logInfo(String message, long startTime) {
        LOGGER.info(LOGGER_INFO_TEMPLATE, message, getExecutionTime(startTime), getCurrentUrl());
    }

    private String getExecutionTime(long startTime) {
        return String.valueOf(System.currentTimeMillis() - startTime);
    }

    private void logException(Exception e, long startTime) {
        String message = e.getMessage();
        if (e instanceof NoSuchElementException) {
            List<String> attributes = Arrays.asList(e.getMessage().split("\n"));
            message = "WebElement is not found: " + attributes.stream()
                    .filter(p -> p.contains("Element info"))
                    .findFirst()
                    .orElse(attributes.get(0));
        }
        LOGGER.error(LOGGER_INFO_TEMPLATE, message, getExecutionTime(startTime), getCurrentUrl());
    }
}
